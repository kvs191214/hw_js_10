<b>Теоретичні питання</b>

1. Які способи JavaScript можна використовувати для створення та додавання нових DOM-елементів?

2. Опишіть покроково процес видалення одного елементу (умовно клас "navigation") зі сторінки.

3. Які є методи для вставки DOM-елементів перед/після іншого DOM-елемента?

****
<b>Відповіді :</b>

1. Для створення та додавання нових DOM-елементів можна використовувати методи :
    - Для створення нових
      вузлів: `document.createElement()`, `document.createTextNode()`, `cloneNode`, `createDocumentFragment`.<br>
      <br>
    - Для
      довавання : `append`, `prepend`, `before`, `after`, `replaceWith`, `insertAdjacentHTML`, `appendChild`, `insertBefore`, `replaceChild`.<br>
      <br>
2. Процесс видалення елементу з класом "navigation" :
    - Спочатку знаходимо елемент з класом "navigation" і додаємо у змінну:<br>
      `const getElemNavigation = document.querySelector('.navigation');`<br>
      <br>
    - потім користуємося методом `remove`:
      `getElemNavigation.remove()`<br>
      <br>
   
3. `before` та `after` для вставлення перед або після елементу за його межами , або за
      допомогою `insertAdjacentHTML("beforebegin")` та `insertAdjacentHTML("afterend")`.
