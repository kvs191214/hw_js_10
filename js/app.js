// 1. Створіть новий елемент <a>, задайте йому текст "Learn More" і атрибут href з посиланням на "#". Додайте цей елемент в footer після параграфу.

const newElemA = document.createElement('a');
const footer = document.querySelector('footer');

newElemA.innerText = "Learn More";
newElemA.setAttribute('href', '#');

footer.append(newElemA);

// 2. Створіть новий елемент <select>. Задайте йому ідентифікатор "rating", і додайте його в тег main перед секцією "Features".
// Створіть новий елемент <option> зі значенням "4" і текстом "4 Stars", і додайте його до списку вибору рейтингу.
// Створіть новий елемент <option> зі значенням "3" і текстом "3 Stars", і додайте його до списку вибору рейтингу.
// Створіть новий елемент <option> зі значенням "2" і текстом "2 Stars", і додайте його до списку вибору рейтингу.
// Створіть новий елемент <option> зі значенням "1" і текстом "1 Star", і додайте його до списку вибору рейтингу.

const main = document.querySelector("main");

const createSelect = document.createElement('select');
createSelect.id = 'rating';
main.prepend(createSelect);


for (let i = 0; i < 4; i++) {
    const createOption = document.createElement("option");
    createOption.value = (i + 1).toString();

    if (i === 0) {
        createOption.innerText = `${i + 1} star`;
    } else {
        createOption.innerText = `${i + 1} stars`;
    };

    createSelect.appendChild(createOption);
}
